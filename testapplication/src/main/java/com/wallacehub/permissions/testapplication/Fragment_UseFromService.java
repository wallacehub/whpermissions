/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.wallacehub.utils.logging.WHLog;


public class Fragment_UseFromService extends Fragment
{
	private static final String TAG = Fragment_UseFromService.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	protected static MyService           m_Service;
	protected        MyServiceConnection m_serviceConnection;


	public Fragment_UseFromService() {
		// Required empty public constructor
	}


	public static Fragment_UseFromService newInstance() {
		return new Fragment_UseFromService();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		m_serviceConnection = new MyServiceConnection();

		final Intent serviceIntent = new Intent(getContext(), MyService.class);

		getActivity().bindService(serviceIntent, m_serviceConnection, Context.BIND_AUTO_CREATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_use_from_service, container, false);
		view.findViewById(R.id.btnRequestPermission).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				m_Service.requestBluetoothPermission();
			}
		});
		return view;
	}


	private class MyServiceConnection implements ServiceConnection
	{
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			WHLog.d(TAG, "Auth Service Connected");

			if (m_Service == null) {
				try {
					m_Service = ((MyService.MyBinder) iBinder).getService();
				}
				catch (ClassCastException e) {
					WHLog.e(TAG, e.toString(), e);
				}
			}
		}


		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			WHLog.d(TAG, "Auth Service Disconnected");
			m_Service = null;
		}
	}
}
