/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;

import static com.wallacehub.permissions.WHPermissions.PermissionState.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_AdvancedUsage extends Fragment
{
	private static final String TAG = Fragment_AdvancedUsage.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.VERBOSE);
	}

	// Data
	private final WHPermissions m_permissions                    = WHPermissions.getInstance();
	final         WHPermission  m_permissionInternet             = m_permissions.getPermission("android.permission.INTERNET");
	//	final         WHPermission    m_permissionNetworkState         = m_permissions.getPermission("android.permission.ACCESS_NETWORK_STATE");
	final         WHPermission  m_permissionWriteExternalStorage = m_permissions.getPermission("android.permission.WRITE_EXTERNAL_STORAGE");
	final         WHPermission  m_permissionAccessCoarseLocation = m_permissions.getPermission("android.permission.ACCESS_COARSE_LOCATION");
//	final         WHPermission    m_permissionManageDocuments      = m_permissions.getPermission("android.permission.MANAGE_DOCUMENTS");

	// Views
	private Button m_btnInternet;
	private Button m_btnNetworkState;
	private Button m_btnExternalStorage;
	private Button m_btnAccessCoarseLocation;
	private Button m_btnManageDocuments;

	private TextView m_txtvwInternetState;
	private TextView m_txtvwNetworkStateState;
	private TextView m_txtvwExternalStorageState;
	private TextView m_txtvwAccessCoarseLocationState;
	private TextView m_txtvwManageDocumentsState;


	public Fragment_AdvancedUsage() {
		// Required empty public constructor
	}


	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @return A new instance of fragment Fragment_TestAllPermissions.
	 */
	// TODO: Rename and change types and number of parameters
	public static Fragment_AdvancedUsage newInstance() {
		Fragment_AdvancedUsage fragment = new Fragment_AdvancedUsage();
		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_advanced_usage, container, false);

		findViews(view);

		setupViews(view);

		return view;
	}


	@Override
	public void onResume() {
		super.onResume();

		final Context context = getActivity();

		m_btnInternet.getBackground().setColorFilter(WHPermissions.permissionDeclaredInManifest(context, m_permissionInternet.getName()) ? 0xFFAAFF00 : 0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		m_btnInternet.setTextColor(Color.BLACK);

//		m_btnNetworkState.getBackground().setColorFilter(WHPermissions.permissionDeclaredInManifest(context, m_permissionNetworkState.getName()) ? 0xFFAAFF00 : 0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
//		m_btnNetworkState.setTextColor(Color.BLACK);

		m_btnExternalStorage.getBackground().setColorFilter(WHPermissions.permissionDeclaredInManifest(context, m_permissionWriteExternalStorage.getName()) ? 0xFFAAFF00 : 0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		m_btnExternalStorage.setTextColor(Color.BLACK);

		m_btnAccessCoarseLocation.getBackground().setColorFilter(WHPermissions.permissionDeclaredInManifest(context, m_permissionAccessCoarseLocation.getName()) ? 0xFFAAFF00 : 0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
		m_btnAccessCoarseLocation.setTextColor(Color.BLACK);

//		m_btnManageDocuments.getBackground().setColorFilter(WHPermissions.permissionDeclaredInManifest(context, m_permissionManageDocuments.getName()) ? 0xFFAAFF00 : 0xFFFFFFFF, PorterDuff.Mode .MULTIPLY);
//		m_btnManageDocuments.setTextColor(Color.BLACK);

		showCurrentPermissionState();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		m_permissions.onRequestPermissionResult(requestCode, permissions, grantResults);
		showCurrentPermissionState();
	}


	@Override
	public void onDetach() {
		super.onDetach();
	}


	private void findViews(final View view) {
		m_btnInternet = (Button) view.findViewById(R.id.btnInternet);
		m_btnNetworkState = (Button) view.findViewById(R.id.btnNetworkState);
		m_btnExternalStorage = (Button) view.findViewById(R.id.btnExternalStorage);
		m_btnAccessCoarseLocation = (Button) view.findViewById(R.id.btnAccessCoarseLocation);
		m_btnManageDocuments = (Button) view.findViewById(R.id.btnManageDocuments);

		m_txtvwInternetState = (TextView) view.findViewById(R.id.txtvwInternet);
		m_txtvwNetworkStateState = (TextView) view.findViewById(R.id.txtvwNetworkState);
		m_txtvwExternalStorageState = (TextView) view.findViewById(R.id.txtvwExternalStorage);
		m_txtvwAccessCoarseLocationState = (TextView) view.findViewById(R.id.txtvwAccessCoarseLocation);
		m_txtvwManageDocumentsState = (TextView) view.findViewById(R.id.txtvwManageDocuments);
	}


	private void setupViews(final View view) {
		m_btnInternet.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				switch (m_permissionInternet.getState()) {
					case PERMISSION_UNDEFINED: {
						m_permissions.requestPermission(m_permissionInternet, 10);
					}
					break;

					case PERMISSION_DENIED: {
						m_permissions.requestPermission(m_permissionInternet, 10);
					}
					break;

					case PERMISSION_DO_NOT_ASK_AGAIN: {

					}
					break;

					case PERMISSION_GRANTED: {

					}
					break;

					case PERMISSION_PENDING: {

					}
					break;
				}
			}
		});

//		m_btnNetworkState.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v) {
//				switch (m_permissionNetworkState.getState()) {
//					case PERMISSION_UNDEFINED: {
//						m_permissions.requestPermission(Fragment_AdvancedUsage.this, m_permissionNetworkState, 10);
//					}
//					break;
//
//					case PERMISSION_DENIED: {
//
//					}
//					break;
//
//					case PERMISSION_DO_NOT_ASK_AGAIN: {
//
//					}
//					break;
//
//					case PERMISSION_GRANTED: {
//
//					}
//					break;
//
//					case PERMISSION_PENDING: {
//
//					}
//					break;
//				}
//			}
//		});

		m_btnExternalStorage.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				switch (m_permissionWriteExternalStorage.getState()) {
					case PERMISSION_UNDEFINED: {
						m_permissions.requestPermission(m_permissionWriteExternalStorage, 10);
					}
					break;

					case PERMISSION_DENIED: {

					}
					break;

					case PERMISSION_DO_NOT_ASK_AGAIN: {

					}
					break;

					case PERMISSION_GRANTED: {

					}
					break;

					case PERMISSION_PENDING: {

					}
					break;
				}

			}
		});

		m_btnAccessCoarseLocation.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				switch (m_permissionAccessCoarseLocation.getState()) {
					case PERMISSION_UNDEFINED: {
						m_permissions.requestPermission(m_permissionAccessCoarseLocation, 10);
					}
					break;

					case PERMISSION_DENIED: {

					}
					break;

					case PERMISSION_DO_NOT_ASK_AGAIN: {

					}
					break;

					case PERMISSION_GRANTED: {

					}
					break;

					case PERMISSION_PENDING: {

					}
					break;
				}

			}
		});

//		m_btnManageDocuments.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v) {
//				switch (m_permissionManageDocuments.getState()) {
//					case PERMISSION_UNDEFINED: {
//						m_permissions.requestPermission(Fragment_AdvancedUsage.this, m_permissionManageDocuments, 10);
//					}
//					break;
//
//					case PERMISSION_DENIED: {
//
//					}
//					break;
//
//					case PERMISSION_DO_NOT_ASK_AGAIN: {
//
//					}
//					break;
//
//					case PERMISSION_GRANTED: {
//
//					}
//					break;
//
//					case PERMISSION_PENDING: {
//
//					}
//					break;
//				}
//
//			}
//		});
	}


	@SuppressLint("SetTextI18n")
	private void showCurrentPermissionState() {
		m_txtvwInternetState.setText(Integer.toString(m_permissionInternet.getState()));
//		m_txtvwNetworkStateState.setText(Integer.toString(m_permissionNetworkState.getState()));
		m_txtvwExternalStorageState.setText(Integer.toString(m_permissionWriteExternalStorage.getState()));
		m_txtvwAccessCoarseLocationState.setText(Integer.toString(m_permissionAccessCoarseLocation.getState()));
//		m_txtvwManageDocumentsState.setText(Integer.toString(m_permissionManageDocuments.getState()));
	}
}
