/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;

/**
 * Test application for the WHPermissions library
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-01-13.
 */
public class ApplicationTest extends Application
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = ApplicationTest.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	@Override
	public void onCreate() {
		super.onCreate();

		initPermissions();
	}


	/**
	 * This adds all of the known permissions to the permission list.
	 */
	protected void initPermissions() {
		final WHPermissions permissions = WHPermissions.init(this);

		permissions.setTestForCorrectness(false);

		PackageInfo packageInfo = null;
		try {
			packageInfo = getPackageManager().getPackageInfo("android", PackageManager.GET_PERMISSIONS);
		}
		catch (PackageManager.NameNotFoundException e) {
			WHLog.e(TAG, e.toString(), e);
		}

		if ((null != packageInfo) && (packageInfo.permissions != null)) {
			// For each defined permission, create a WHPermission object
			for (PermissionInfo permissionInfo : packageInfo.permissions) {
				WHLog.d(TAG, permissionInfo.name);
				final WHPermission permission = new WHPermission(permissionInfo.name, "My reasons are my own!!");
				permission.setPermissionInfo(permissionInfo);

				permissions.addPermission(permission);
			}
		}
	}
}
