/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;
import androidx.annotation.Nullable;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;


public class MyService extends Service
{
	private static final String TAG = MyService.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	private MyBinder m_binder;


	public MyService() {
		super();

		m_binder = new MyBinder();
	}


	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return m_binder;
	}


	protected void requestBluetoothPermission() {
		WHPermissions permissions = WHPermissions.getInstance();
		WHPermission bluetoothPermission = permissions.getPermission(Manifest.permission.BLUETOOTH);

		permissions.usePermission(bluetoothPermission, new WHPermissions.SimplePermissionCallbacks()
		{
			@Override
			public void onPermissionsGranted() {
				Toast.makeText(MyService.this, "WHPermission is GRANTED", Toast.LENGTH_SHORT).show();
			}


			@Override
			public void onPermissionsPending() {
				Toast.makeText(MyService.this, "WHPermission is PENDING", Toast.LENGTH_SHORT).show();
			}


			@Override
			public void onPermissionsDenied() {
				Toast.makeText(MyService.this, "WHPermission is DENIED", Toast.LENGTH_SHORT).show();
			}
		});
	}


	public class MyBinder extends Binder
	{
		public MyService getService() {
			return MyService.this;
		}
	}
}
