/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_TestAllPermissions#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_TestAllPermissions extends Fragment
{
	private static final String TAG = Fragment_TestAllPermissions.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.VERBOSE);
	}

	private final WHPermissions      m_permissions = WHPermissions.getInstance();
	private       AdapterPermissions m_adapterPermissions;


	public Fragment_TestAllPermissions() {
		// Required empty public constructor
	}


	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @return A new instance of fragment Fragment_TestAllPermissions.
	 */
	// TODO: Rename and change types and number of parameters
	public static Fragment_TestAllPermissions newInstance() {
		Fragment_TestAllPermissions fragment = new Fragment_TestAllPermissions();
		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_test_all_permissions, container, false);

		findViews(view);

		setupViews(view);

		return view;
	}


	private void findViews(final View view) {
	}


	private void setupViews(final View view) {

		final Context context = getActivity();

		// Construct the data source
		List<WHPermission> permissions = createPermissionList(context);

		// Create the adapter to convert the array to views
		m_adapterPermissions = new AdapterPermissions(getActivity(), permissions);

		// Attach the adapter to a ListView
		ListView listView = (ListView) view.findViewById(R.id.lstvw);
		listView.setAdapter(m_adapterPermissions);
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		m_permissions.onRequestPermissionResult(requestCode, permissions, grantResults);

		m_adapterPermissions.notifyDataSetChanged();
	}


	private List<WHPermission> createPermissionList(@NonNull final Context context) {
		final List<WHPermission> permissionList = new ArrayList<>();

		PackageInfo packageInfo = null;
		try {
			packageInfo = context.getPackageManager().getPackageInfo("android", PackageManager.GET_PERMISSIONS);
		}
		catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		final WHPermissions permissions = WHPermissions.getInstance();

		if ((null != packageInfo) && (packageInfo.permissions != null)) {
			// For each defined permission, create a WHPermission object
			for (PermissionInfo permissionInfo : packageInfo.permissions) {
				final WHPermission permission = permissions.getPermission(permissionInfo.name);

				permissionList.add(permission);
			}
		}

		return permissionList;
	}
}
