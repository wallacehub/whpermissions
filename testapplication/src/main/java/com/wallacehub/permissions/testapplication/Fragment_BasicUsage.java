/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;


import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;

import static android.content.Context.LOCATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_BasicUsage extends Fragment
{
	private static final String TAG = Fragment_BasicUsage.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.VERBOSE);
	}

	// Data
	private final WHPermissions m_permissions                    = WHPermissions.getInstance();
	final         WHPermission  m_permissionAccessCoarseLocation = m_permissions.getPermission("android.permission.ACCESS_COARSE_LOCATION");

	// Views
	private Button   m_btnTest;
	private TextView m_txtvwState;
	private TextView m_txtvwLocation;


	public Fragment_BasicUsage() {
		// Required empty public constructor
	}


	public static Fragment_BasicUsage newInstance() {
		Fragment_BasicUsage fragment = new Fragment_BasicUsage();
		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_basic_usage, container, false);

		findViews(view);

		setupViews(view);

		return view;
	}


	@Override
	public void onResume() {
		super.onResume();

		updateState();
	}


	private void findViews(final View view) {
		m_btnTest = (Button) view.findViewById(R.id.btnTest);
		m_txtvwLocation = (TextView) view.findViewById(R.id.txtvwLocation);
		m_txtvwState = (TextView) view.findViewById(R.id.txtvwState);
	}


	private void setupViews(final View view) {
		m_btnTest.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				m_permissions.usePermission(m_permissionAccessCoarseLocation, new WHPermissions.SimplePermissionCallbacks()
				{
					@Override
					public void onPermissionsGranted() {
						LocationManager m_location_manager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
						//noinspection MissingPermission
						Location lm = m_location_manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						m_txtvwLocation.setText(lm.toString());

						updateState();
					}


					@Override
					public void onPermissionsPending() {
						m_txtvwLocation.setText(R.string.location_unknown);

						updateState();
					}


					@Override
					public void onPermissionsDenied() {
						m_txtvwLocation.setText(R.string.location_unknown);

						updateState();
					}
				});

			}
		});
	}


	private void updateState() {
		switch (m_permissionAccessCoarseLocation.getState()) {
			case WHPermissions.PermissionState.PERMISSION_UNDEFINED: {
				m_txtvwState.setText(R.string.unknown);
			}
			break;

			case WHPermissions.PermissionState.PERMISSION_GRANTED: {
				m_txtvwState.setText(R.string.granted);
			}
			break;

			case WHPermissions.PermissionState.PERMISSION_DENIED: {
				m_txtvwState.setText(R.string.denied);
			}
			break;

			case WHPermissions.PermissionState.PERMISSION_DO_NOT_ASK_AGAIN: {
				m_txtvwState.setText(R.string.do_not_ask_again);
			}
			break;

			case WHPermissions.PermissionState.PERMISSION_PENDING: {
				m_txtvwState.setText(R.string.pending);
			}
			break;
		}

	}
}
