/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.wallacehub.utils.logging.WHLog;

import java.util.ArrayList;
import java.util.List;


public class Activity_Main extends AppCompatActivity
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = Activity_Main.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		final MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
		final ViewPager pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);
	}


	/**
	 * Simple adapter class to switch from one Fragment to the other.
	 */
	private static class MyAdapter extends FragmentPagerAdapter
	{
		private static List<Pages> m_pages = new ArrayList<>(4);

		MyAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);

			m_pages.add(new Pages("Basic usage", Fragment_BasicUsage.newInstance()));
			m_pages.add(new Pages("Advanced usage", Fragment_AdvancedUsage.newInstance()));
			m_pages.add(new Pages("Use from a Service", Fragment_UseFromService.newInstance()));
			m_pages.add(new Pages("Test all Permissions", Fragment_TestAllPermissions.newInstance()));
			notifyDataSetChanged();
		}


		@Override
		public int getCount() {
			return m_pages.size();
		}


		@Override
		public Fragment getItem(int position) {
			return m_pages.get(position).fragment;
		}


		@Override
		public CharSequence getPageTitle(int position) {
			return m_pages.get(position).name;
		}

		private class Pages
		{
			String   name;
			Fragment fragment;


			Pages(String name, Fragment fragment) {
				this.name = name;
				this.fragment = fragment;
			}
		}
	}
}