/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions.testapplication;

import android.app.Activity;
import android.content.pm.PermissionInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import androidx.annotation.NonNull;
import com.wallacehub.permissions.WHPermission;
import com.wallacehub.permissions.WHPermissions;
import com.wallacehub.utils.logging.WHLog;

import java.util.List;

import static com.wallacehub.permissions.WHPermissions.PermissionState.*;


/**
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-01-16.
 */
public class AdapterPermissions extends ArrayAdapter<WHPermission>
{
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	private static final String TAG = AdapterPermissions.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}

	// Data
	private Activity m_activity;


	//<editor-fold desc="Constructors">
	public AdapterPermissions(Activity activity, List<WHPermission> permissions) {
		super(activity, 0, permissions);

		m_activity = activity;
	}
	//</editor-fold>


	@NonNull
	public View getView(int position, View convertView, @NonNull final ViewGroup parent) {
		// Get the data item for this position
		final WHPermission permission = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_permission, parent, false);
		}

		// Lookup view for data population
		final CheckedTextView txtvwPermissionName = (CheckedTextView) convertView.findViewById(R.id.txtvwPermissionName);
		final Button btnToggleState = (Button) convertView.findViewById(R.id.btnToggleState);
		btnToggleState.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				final WHPermissions permissions = WHPermissions.getInstance();
				permissions.requestPermission(permission, 9);
			}
		});

		// Populate the data into the template view using the data object
		final String name = permission.getName().substring(permission.getName().lastIndexOf('.') + 1);
		txtvwPermissionName.setText(name);

		final int permissionInfo = permission.getPermissionInfo().protectionLevel;
		txtvwPermissionName.setBackgroundColor(permissionInfo == PermissionInfo.PROTECTION_DANGEROUS ? Color.RED : Color.WHITE);
		txtvwPermissionName.setTextColor(permissionInfo == PermissionInfo.PROTECTION_DANGEROUS ? Color.WHITE : Color.BLACK);
		txtvwPermissionName.setChecked(WHPermissions.permissionDeclaredInManifest(m_activity, permission.getName()));

		switch (permission.getState()) {
			case PERMISSION_UNDEFINED: {
				btnToggleState.setText("Undefined");
				btnToggleState.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
				btnToggleState.setTextColor(Color.BLACK);
			}
			break;

			case PERMISSION_DENIED: {
				btnToggleState.setText("Denied");
				btnToggleState.getBackground().setColorFilter(0xFFFFAAAA, PorterDuff.Mode.MULTIPLY);
				btnToggleState.setTextColor(Color.BLACK);
			}
			break;

			case PERMISSION_DO_NOT_ASK_AGAIN: {
				btnToggleState.setText("Do not ask again");
				btnToggleState.getBackground().setColorFilter(0xFFFFAAAA, PorterDuff.Mode.MULTIPLY);
				btnToggleState.setTextColor(Color.BLACK);
			}
			break;

			case PERMISSION_GRANTED: {
				btnToggleState.setText("Granted");
				btnToggleState.getBackground().setColorFilter(0xFFAAFFAA, PorterDuff.Mode.MULTIPLY);
				btnToggleState.setTextColor(Color.BLACK);
			}
			break;

			case PERMISSION_PENDING: {
				btnToggleState.setText("Pending");
				btnToggleState.getBackground().setColorFilter(0xFFAAAAAA, PorterDuff.Mode.MULTIPLY);
				btnToggleState.setTextColor(Color.BLACK);
			}
			break;
		}

		return convertView;
	}
}
