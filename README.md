

WHPermissions is a wrapper library used to simplify basic Android system permissions logic when targeting Android SDK 23 and higher.

#Benefits

WHPermissions simplifies the flow of permission management and does some behind the scenes work for you.
- Simplifies interacting with Android for permission requests and status
- Simplifies permission rationale by declaring it once.
- It simplifies interfacing with the user by managing the rationale dialog flow.
- It will cache permission state as much as possible to simplify and speed up dealing with permissions
- Can perform sanity checks in debug mode to ensure that you are using permissions properly
- Has two modes of usage : basic mode and advanced
- Help with permissions in general.


#Installation

WHPermissions is installed by adding the following dependency to your build.gradle file, using the latest version number as indicated on the badge :

[ ![Download](https://api.bintray.com/packages/mikewallacedev/maven/WHPermissions/images/download.svg) ](https://bintray.com/mikewallacedev/maven/WHPermissions/_latestVersion)

```groovy
    dependencies {
    	compile 'com.wallacehub.permissions:WHPermissions:x.x.x'
    }

```

#Usage

##Defining the permissions that you use

1. You must call `WHPermissions.init(this)` in the Application class' `onCreate()` method.
2. All of your permissions should be declared in an Application class. WHPermissions uses a singleton object to keep track of permissions. You must always declare your permissions in the application object no matter which technique (basic or advanced) you use to verify your permissions.
```java
public class DemoApplication extends Application
{
	@Override
   public void onCreate() {
      super.onCreate();

      final WHPermissions permissions = WHPermissions.init(this);

      permissions.addPermission(new Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE, "Reason"));
      permissions.addPermission(new Permission(Manifest.permission.ACCESS_FINE_LOCATION, "Reason"));
      permissions.addPermission(new Permission(Manifest.permission.ACCESS_COARSE_LOCATION, "Reason"));
   }
}
```

##Basic

Tell the permissions manager that you wish to use a permission. It will handle it and call you back once all the user interfacing is done.

This will work for most situations.

It is not recommended for background processing as the situation can arise where a permission was requested by some other part of the app and is still pending.

1. Get a reference to the permission manager.
1. Use the permission if granted.

```java
public class MainActivity extends AppCompatActivity
{
	// Permission management
	private final WHPermissions m_permissions        = WHPermissions.getInstance();
	private final WHPermission    m_permissionLocation = m_permissions.getPermission(Manifest.permission.ACCESS_FINE_LOCATION);

  // Example call when used with Google Maps
	@Override
	public void onMapReady(GoogleMap googleMap) {
		m_permissions.usePermission(m_permissionLocation, new PermissionsCallbacks() {

			void onPermissionsGranted(int requestCode, Permission permission) {
				m_googleMap.setMyLocationEnabled(true);		
			}

			void onPermissionsDenied(int requestCode, Permission permission) {
				// Do whatever you want to do if the use refuses the request.
		}
	}
}
```

That's it. The library will take care of everything, including presenting the user with a rationale if needed.

#Advanced Usage

Ask for permissions when required.

You should always verify that you still have a permission when you use a restricted Android API.

1. Implement the ```PermissionCallbacks``` interface.
1. Set the object as a permission listener in ```onCreate()```.
1. Remove the object as a permission listener in ```onDestroy()```.
2. Get a reference to the permission manager.
3. Check if the permission is granted before using it.
4. Request it if needed.
5. Respond to the request.

```java
public class MainActivity extends AppCompatActivity implements PermissionCallbacks
{

	private static final int  REQUEST_PERMISSION_READ_EXTERNAL = 100;

	// Permission management
	private final WHPermissions m_permissions        = WHPermissions.getInstance();
	private final WHPermission    m_permissionLocation = m_permissions.getPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		m_permissions.addPermissionCallbacks(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		m_permissions.removePermissionCallbacks(this);
	}

	@Override
	public void onClickedSaveOrWhatever() {
		if (m_permissionLocation.isGranted()) {
			saveFileToDisk();
		}
		else {
			m_permissions.requestPermission(this, m_permissionLocation, REQUEST_PERMISSION_READ_EXTERNAL);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		// ALWAYS send the response to the permission manager
		m_permissions.onRequestPermissionResult(requestCode, permissions, grantResults);
	}


	void onPermissionsGranted(int requestCode, Permission permission) {
		switch (requestCode) {
			case  REQUEST_PERMISSION_READ_EXTERNAL :
				saveFileToDisk();		
			break;
		}
	}

	void onPermissionsDenied(int requestCode, Permission permission) {
		switch (requestCode) {
			case  REQUEST_PERMISSION_READ_EXTERNAL :
				// Do whatever you want to do if the use refuses the request.
			break;
		}
	}

  void saveFileToDisk() {
    // Save your file to disk code goes here.
  }
}
```
