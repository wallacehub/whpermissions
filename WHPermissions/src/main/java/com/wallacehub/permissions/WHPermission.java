/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions;

import android.content.pm.PermissionInfo;
import androidx.annotation.NonNull;
import com.wallacehub.permissions.WHPermissions.PermissionState;
import com.wallacehub.utils.logging.WHLog;

/**
 * A class that represents a permission and its states
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-01-13.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class WHPermission
{
	private static final String TAG = WHPermission.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}


	//<editor-fold desc="Data">
	// Data
	private final String         m_name;
	private       String         m_permissionRationale;
	@PermissionState
	private       int            m_state;
	private       PermissionInfo m_permissionInfo;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	@SuppressWarnings("unused")
	private WHPermission() { throw new Exception_Permission("Cannot use default constructor"); }


	/**
	 * Constructor
	 *
	 * @param permissionName Android WHPermission like Manifest.permission.CAMERA or Manifest.permission.ACCESS_FINE_LOCATION
	 */
	public WHPermission(@NonNull final String permissionName) {
		this(permissionName, "");
	}


	/**
	 * Constructor
	 *
	 * @param permissionName Android WHPermission like Manifest.permission.CAMERA or Manifest.permission.ACCESS_FINE_LOCATION
	 */
	public WHPermission(@NonNull final String permissionName, @NonNull final String permissionRationale) throws Exception_Permission {
		this(permissionName, permissionRationale, PermissionState.PERMISSION_UNDEFINED);
	}


	/**
	 * Constructor
	 *
	 * @param permissionName Android WHPermission like Manifest.permission.CAMERA or Manifest.permission.ACCESS_FINE_LOCATION
	 */
	public WHPermission(@NonNull final String permissionName, @NonNull final String permissionRationale, final int state) throws Exception_Permission {
		super();

		m_name = permissionName;
		m_permissionRationale = permissionRationale;
		m_state = state;
	}
	//</editor-fold>


	//<editor-fold desc="Getters-Setters">
	public String getName() {
		return m_name;
	}


	public String getPermissionRationale() {
		return m_permissionRationale;
	}


	public void setPermissionRationale(final String permissionRationale) {
		m_permissionRationale = permissionRationale;
	}


	@PermissionState
	public int getState() {
		return m_state;
	}


	public void setState(@PermissionState final int state) {
		m_state = state;
	}


	public PermissionInfo getPermissionInfo() {
		return m_permissionInfo;
	}


	public void setPermissionInfo(final PermissionInfo permissionInfo) {
		m_permissionInfo = permissionInfo;
	}

	//</editor-fold>


	/**
	 * @return true if the WHPermission is ignored by the system
	 */
	public boolean isIgnored() {
		return m_state == PermissionState.PERMISSION_IGNORE;
	}


	/**
	 * @return true if the WHPermission is already Denied
	 */
	public boolean isPending() {
		return m_state == PermissionState.PERMISSION_PENDING;
	}


	/**
	 * @return true if the WHPermission is already Granted. This does not include ignored permissions
	 */
	public boolean isGranted() {
		final boolean isGranted = PermissionState.PERMISSION_GRANTED == m_state;

		WHLog.d(TAG, "isGranted = " + isGranted);

		return isGranted;
	}


	/**
	 * @return true if the WHPermission is already Granted. This does not include ignored permissions
	 */
	public boolean isGrantedOrIgnored() {
		final boolean isGranted = PermissionState.PERMISSION_GRANTED == m_state;
		final boolean isIgnored = PermissionState.PERMISSION_IGNORE == m_state;

		WHLog.d(TAG, "isGrantedOrIgnored = " + (isGranted || isIgnored));

		return isGranted || isIgnored;
	}


	/**
	 * @return true if the WHPermission is already Denied.  This does not include ignored persmissions
	 */
	public boolean isDenied() {
		return (m_state == PermissionState.PERMISSION_DENIED || m_state == PermissionState.PERMISSION_DO_NOT_ASK_AGAIN);
	}


	//<editor-fold desc="Equals-Hashcode-ToString">
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if ((o == null) || (getClass() != o.getClass())) {
			return false;
		}

		final WHPermission that = (WHPermission) o;

		if (!getName().equals(that.getName())) {
			return false;
		}

		if (getState() != that.getState()) {
			throw new Exception_Permission("Objects in invalid state!");
		}

		return true;
	}


	@Override
	public int hashCode() {
		int result = getName().hashCode();
		result = 31 * result + getState();
		return result;
	}


	@Override
	public String toString() {
		return "WHPermission { " +
				"m_name = '" + m_name +
				", m_state = " + m_state +
				", m_permissionRationale = " + m_permissionRationale +
				", m_permissionInfo = " + m_permissionInfo +
				" }";
	}
	//</editor-fold>
}
