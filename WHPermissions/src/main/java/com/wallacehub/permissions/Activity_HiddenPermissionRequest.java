/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.wallacehub.utils.exceptions.Exception_RequiredParameter;
import com.wallacehub.utils.logging.WHLog;

import static com.wallacehub.permissions.WHPermissions.PermissionState.PERMISSION_DO_NOT_ASK_AGAIN;

/**
 * This activity is used to silently ask Android for permissions. It allows me to bypass
 * the user's activity and make the API simpler.
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-02-20.
 */
public class Activity_HiddenPermissionRequest extends AppCompatActivity
{
	public static final String TAG = Activity_HiddenPermissionRequest.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}

	static final String EXTRA_PERMISSION     = TAG + ".EXTRA_PERMISSION";
	static final String EXTRA_REQUEST_NUMBER = TAG + ".EXTRA_REQUEST_NUMBER";


	public static void start(@NonNull final Context context, @NonNull final String permissionName, int requestNumber) {

		if (TextUtils.isEmpty(permissionName)) {
			WHLog.e(TAG, "WHPermission name is empty");
			return;
		}

		final Intent intent = new Intent(context, Activity_HiddenPermissionRequest.class);
		intent.putExtra(EXTRA_PERMISSION, permissionName);
		intent.putExtra(EXTRA_REQUEST_NUMBER, requestNumber);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		context.startActivity(intent);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final String permissionName = getIntent().getStringExtra(EXTRA_PERMISSION);
		if (null == permissionName) {
			throw new Exception_RequiredParameter("No permission name found in intent.  Permission name is required.");
		}

		int requestCode = getIntent().getIntExtra(EXTRA_REQUEST_NUMBER, -1);
		if (-1 == requestCode) {
			throw new Exception_RequiredParameter("No request number found in intent.  Request number name is required.");
		}

		final WHPermission permission = WHPermissions.getInstance().getPermission(permissionName);

		if (null == permission) {
			WHLog.e(TAG, "WHPermission not found. Exiting.");
			setResult(RESULT_CANCELED);
			finish();
		}

		final boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permissionName);

		if (!showRationale && permission.isDenied()) {
			WHLog.d(TAG, "Setting state to PERMISSION_DO_NOT_ASK_AGAIN");
			permission.setState(PERMISSION_DO_NOT_ASK_AGAIN);

			// I have no clue why I changed this code. It makes no sense to me.
			// So if future me understands why I did this, good or bad, put a comment saying why and remove these comments.
//			<<---------
//			setResult(RESULT_CANCELED);
//			finish();
			ActivityCompat.requestPermissions(this, new String[]{permissionName}, requestCode);
//			--------->>
		}
		else if (showRationale) {
			WHLog.d(TAG, "WHPermission should Show rationale.");

			if ((null != permission.getPermissionRationale())) {
				WHLog.d(TAG, "Rationale is declared, showing.");
				showRationaleAlertDialog(permission, requestCode, 0, 0);
			}
			else {
				WHLog.d(TAG, "Rationale is NOT declared.");
				permission.setState(WHPermissions.PermissionState.PERMISSION_DENIED);
				setResult(RESULT_CANCELED);
				finish();
			}
		}
		else {
			ActivityCompat.requestPermissions(this, new String[]{permissionName}, requestCode);
		}
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		WHPermissions.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);

		finish();
	}


	/**
	 * Show an alert dialog explaining permission request rationale.
	 */
	private void showRationaleAlertDialog(@NonNull final WHPermission permission, final int requestCode, @StringRes int positiveButton, @StringRes int negativeButton) {
		WHLog.d(TAG, "showRationaleAlertDialog");
		WHLog.d(TAG, "permission = [ " + permission + " ] \n" +
				"requestCode = [ " + requestCode + " ] \n" +
				"positiveButton = [ " + positiveButton + " ] \n" +
				"negativeButton = [ " + negativeButton + " ] \n");


		final AlertDialog dialog = new AlertDialog.Builder(this)
				.setMessage(permission.getPermissionRationale())
				.setCancelable(false)
				.setMessage(permission.getPermissionRationale())
				.setTitle(android.R.string.dialog_alert_title)
				.setPositiveButton(positiveButton != 0 ? positiveButton : android.R.string.ok, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which) {
						ActivityCompat.requestPermissions(Activity_HiddenPermissionRequest.this, new String[]{permission.getName()}, requestCode);
					}
				})
				.setNegativeButton(negativeButton != 0 ? negativeButton : android.R.string.cancel, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which) {
						WHPermissions.getInstance().onRequestPermissionResult(requestCode, new String[]{permission.getName()}, new int[]{PackageManager.PERMISSION_DENIED});

						Activity_HiddenPermissionRequest.this.finish();
					}
				}).create();

		dialog.show();
	}
}