/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 Michael Wallace, Wallace Hub Software
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.wallacehub.permissions;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.wallacehub.utils.Utils;
import com.wallacehub.utils.logging.WHLog;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.*;

import static com.wallacehub.permissions.WHPermissions.PermissionState.*;

/**
 * Component that manages permissions and their states
 *
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 2017-01-13.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class WHPermissions
{

	//<editor-fold desc="TAG">
	private static final String TAG = WHPermissions.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.VERBOSE);
	}
	//</editor-fold>


	//<editor-fold desc="Interfaces">
	public interface SimplePermissionCallbacks
	{
		void onPermissionsIgnored();

		void onPermissionsGranted();

		void onPermissionsPending();

		void onPermissionsDenied();
	}


	public interface PermissionCallbacks extends ActivityCompat.OnRequestPermissionsResultCallback
	{
		void onPermissionsIgnored(int requestCode, WHPermission permission);

		void onPermissionsGranted(int requestCode, WHPermission permission);

		void onPermissionsDenied(int requestCode, WHPermission permission);
	}


	public interface PermissionCallbacksSingle extends ActivityCompat.OnRequestPermissionsResultCallback
	{
		void onPermissionsResult(int requestCode, List<WHPermission> permissions);
	}
	//</editor-fold>

	//<editor-fold desc="Data Types (Enums)">


	/**
	 * WHPermission State
	 */
	public
	@Retention(RetentionPolicy.SOURCE)
	@IntDef({PERMISSION_UNDEFINED, PERMISSION_IGNORE, PERMISSION_PENDING, PERMISSION_GRANTED, PERMISSION_DENIED, PERMISSION_DO_NOT_ASK_AGAIN})
	@interface PermissionState
	{
		int PERMISSION_UNDEFINED        = 0;
		int PERMISSION_IGNORE           = 1;  // Permission added to WHPermissions for compatibility but should be ignored. The permission is most likely not declared in the manifest.
		int PERMISSION_PENDING          = 2;
		int PERMISSION_GRANTED          = 3;
		int PERMISSION_DENIED           = 4;
		int PERMISSION_DO_NOT_ASK_AGAIN = 5;
	}
	//</editor-fold>

	//<editor-fold desc="Data">
	// Data
	private static WHPermissions INSTANCE = null;

	private final Map<String, WHPermission>              m_permissionMap                = new HashMap<>(5);
	private       List<PermissionCallbacks>              m_permissionCallbacks          = new ArrayList<>(3);
	private       List<PermissionCallbacksSingle>        m_permissionCallbacksSingle    = new ArrayList<>(3);
	private final SparseArray<SimplePermissionCallbacks> m_simplePermissionCallbacksMap = new SparseArray<>();
	private       boolean                                m_testForCorrectness;
	private       Context                                m_context;

	//</editor-fold>


	//<editor-fold desc="Initialization">
	// Initialization
	public static WHPermissions init(@NonNull final Context context) {
		if (null == INSTANCE) {
			INSTANCE = new WHPermissions(context);
		}
		return INSTANCE;
	}


	public static WHPermissions getInstance() {
		if (null == INSTANCE) {
			throw new Exception_Permission("WHPermissions has not been correctly initialized. You MUST called init(context) first.");
		}
		return INSTANCE;
	}


	/**
	 * Private constructor
	 */
	private WHPermissions(@NonNull final Context context) {
		m_testForCorrectness = true;
		m_context = context.getApplicationContext();
	}
	//</editor-fold>


	//<editor-fold desc="Getters-Setters">
	// Getters-Setters
	public boolean isTestForCorrectness() {
		return m_testForCorrectness;
	}


	public void setTestForCorrectness(boolean testForCorrectness) {
		m_testForCorrectness = testForCorrectness;
	}
	//</editor-fold>


	/**
	 * @param permissionCallbacks
	 */
	public void addPermissionCallbacks(PermissionCallbacks permissionCallbacks) {
		m_permissionCallbacks.add(permissionCallbacks);
	}


	/**
	 * @param permissionCallbacks
	 */
	public void removePermissionCallbacks(PermissionCallbacks permissionCallbacks) {
		m_permissionCallbacks.remove(permissionCallbacks);
	}


	/**
	 * @param permissionCallbacksSingle
	 */
	public void addPermissionCallbacksSingle(PermissionCallbacksSingle permissionCallbacksSingle) {
		m_permissionCallbacksSingle.add(permissionCallbacksSingle);
	}


	/**
	 * @param permissionCallbacksSingle
	 */
	public void removePermissionCallbacksSingle(PermissionCallbacksSingle permissionCallbacksSingle) {
		m_permissionCallbacksSingle.remove(permissionCallbacksSingle);
	}


	/**
	 * Adds a permission that is used by the application. All the permissions are cached for easy/quick reference
	 *
	 * @param permission The permission to add.
	 */
	public void addPermission(@NonNull final WHPermission permission) {
		WHLog.d(TAG, "addPermission");
		WHLog.d(TAG, "permission = [ " + permission + " ] \n");

		if ((null == permission) || (null == permission.getName())) {
			throw new Exception_Permission("WHPermission can not be null and must have a name");
		}

		// Do some helpful testing to help out the devs. I'm nice like that.
		// I can't use BuildConfig.Debug Since a library is always considered to be in release mode
		if (m_testForCorrectness && (0 != ((m_context.getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)))) {
			try {
				if (!permission.isIgnored() && !needsRequesting(permission)) {
					WHLog.e(TAG, "WHPermission [ " + permission.getName() + " ] does NOT require requesting");
					WHLog.e(TAG, "Do not add it to the Permissions Manager.");
					WHLog.d(TAG, "You can turn off this warning by calling setTestForCorrectness(false)");

					throw new Exception_Permission("This is a helper exception thrown by WHPermissions for your convenience : WHPermission [ " + permission.getName() + " ] " +
							                               "does NOT require requesting.  Do not add it to the Permissions Manager.");
				}
			}
			catch (PackageManager.NameNotFoundException e) {
				WHLog.e(TAG, e.toString(), e);
			}

			if (!permission.isIgnored() && !permissionDeclaredInManifest(m_context, permission.getName())) {
				WHLog.e(TAG, "WHPermission [ " + permission.getName() + " ] is not in the manifest.");
				WHLog.d(TAG, "You can turn off this warning by calling setTestForCorrectness(false)");

				throw new Exception_Permission("This is a helper exception thrown by WHPermissions for your convenience : WHPermission [ " + permission.getName() + " ] " +
						                               "was added to the Permissions Manager but is not in the Manifest.");
			}
		}

		if (hasPermissions(permission.getName())) {
			permission.setState(PERMISSION_GRANTED);
		}

		m_permissionMap.put(permission.getName(), permission);
	}


	@Nullable
	public WHPermission getPermission(@NonNull final String permissionName) {
		WHLog.d(TAG, "getPermission");
		WHLog.d(TAG, "permissionName = [ " + permissionName + " ] \n");

		final WHPermission permission = m_permissionMap.get(permissionName);

		if (null == permission) {
			WHLog.e(TAG, "WHPermission [" + permissionName + "] was not added to WHPermissions");
		}

		return permission;
	}
	//</editor-fold>


	/**
	 * Check if the calling context has a set of permissions.
	 *
	 * @param permissonName a permission, such as {@code android.Manifest.permission.CAMERA}.
	 * @return true if all permissions are already granted, false if at least one permission is not yet granted.
	 */
	public boolean hasPermissions(@NonNull final String permissonName) {
		return hasPermissions(m_context, new String[]{permissonName});
	}


	/**
	 * Check if the calling context has a set of permissions.
	 *
	 * @param permissonName a permission, such as {@code android.Manifest.permission.CAMERA}.
	 * @return true if all permissions are already granted, false if at least one permission is not yet granted.
	 */
	public static boolean hasPermissions(@NonNull final Context context, @NonNull final String permissonName) {
		return hasPermissions(context, new String[]{permissonName});
	}


	/**
	 * Check if the calling context has a set of permissions.
	 *
	 * @param perms one ore more permissions, such as {@code android.Manifest.permission.CAMERA}.
	 * @return true if all permissions are already granted, false if at least one permission is not yet granted.
	 */
	public static boolean hasPermissions(@NonNull final Context context, @NonNull final String... perms) {
		WHLog.d(TAG, "hasPermissions");
		WHLog.d(TAG, "context = [ " + context + " ] \nperms = [ " + Arrays.toString(perms) + " ] \n");

		// Always return true for SDK < M, let the system deal with the permissions
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			Log.w(TAG, "hasPermissions: API version < M, returning true by default");
			return true;
		}

		for (final String perm : perms) {
			// this method is called when adding a WHPermission to WHPermissions, at that point in time the permission
			// is obviously not in the cached permission set so it returns null. This is ok.
			final WHPermission permission = WHPermissions.getInstance().getPermission(perm);
			if (null != permission) {
				if (!permission.isIgnored() && !(ContextCompat.checkSelfPermission(context, perm) == PackageManager.PERMISSION_GRANTED)) {
					Log.d(TAG, "application does NOT have permission");
					return false;
				}
			}
			else {
				if (!(ContextCompat.checkSelfPermission(context, perm) == PackageManager.PERMISSION_GRANTED)) {
					Log.d(TAG, "application does NOT have permission");
					return false;
				}
			}
		}

		Log.d(TAG, "application DOES have permission");
		return true;
	}


	/**
	 * Requests an Android permission. The response will be send to the onRequestPermissionsResult method of the mActivity
	 *
	 * @param requestCode user code that could be useful to differentiate several requests
	 * @return true if the permission was requested. False if the platform does not support permission requests or the permission is already granted.
	 */
	public boolean requestPermission(@NonNull final Object object, @NonNull final String permissionName, int requestCode) {
		final WHPermission permission = getPermission(permissionName);

		return requestPermission(permission, requestCode);
	}


	/**
	 * Requests an Android permission. The response will be send to the onRequestPermissionsResult method of the mActivity
	 *
	 * @param requestCode user code that could be useful to differentiate several requests
	 * @return true if onRequestPermissionResult will be called. False if no call is made and the user can just check the permission's state immediately
	 */
	public boolean requestPermission(@NonNull final WHPermission permission, int requestCode) {

		if (null == permission) {
			throw new Exception_Permission("WHPermission must not be null.");  // NON-NLS
		}

		WHLog.d(TAG, "requestPermission");
		WHLog.d(TAG, "permission = [ " + permission.getName() + " ] \nrequestCode = [ " + requestCode + " ] \n");

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			WHLog.d(TAG, "Android version below M, permission is automatically granted");
			permission.setState(PERMISSION_GRANTED);
			onRequestPermissionResult(requestCode, new String[]{permission.getName()}, new int[]{PackageManager.PERMISSION_GRANTED});
			return true;
		}

		// Bypass requesting if not needed.
		try {
			if (!needsRequesting(permission.getName())) {
				permission.setState(PERMISSION_GRANTED);
				onRequestPermissionResult(requestCode, new String[]{permission.getName()}, new int[]{PackageManager.PERMISSION_GRANTED});
				return true;
			}
		}
		catch (PackageManager.NameNotFoundException e) {
			WHLog.e(TAG, e.toString(), e);
		}

		Activity_HiddenPermissionRequest.start(m_context, permission.getName(), requestCode);

		return true;
	}


	/**
	 * Requests an Android permission and use it immediately. A callback will let you know if it is granted or denied.
	 */
	public void usePermission(@NonNull final WHPermission permission, final SimplePermissionCallbacks callback) {

		if (null == permission) {
			throw new Exception_Permission("WHPermission must not be null.");  // NON-NLS
		}

		WHLog.d(TAG, "requestPermission");
		WHLog.d(TAG, "permission = [ " + permission.getName() + " ]");

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			WHLog.d(TAG, "Android version below M, permission is automatically granted");
			permission.setState(PERMISSION_GRANTED);
			callback.onPermissionsGranted();
			return;
		}

		if (permission.isGranted()) {
			WHLog.d(TAG, "WHPermission is already granted");
			callback.onPermissionsGranted();
			return;
		}

		if (permission.isPending()) {
			WHLog.d(TAG, "WHPermission is pending");
			callback.onPermissionsPending();
			return;
		}


		if (permission.isIgnored()) {
			WHLog.d(TAG, "WHPermission is ignored");
			callback.onPermissionsIgnored();
			return;
		}


		// Bypass requesting if not needed.
		try {
			if (!needsRequesting(permission.getName())) {
				permission.setState(PERMISSION_GRANTED);
				callback.onPermissionsGranted();
				return;
			}
		}
		catch (PackageManager.NameNotFoundException e) {
			WHLog.e(TAG, e.toString(), e);
			callback.onPermissionsDenied();
			return;
		}

		final int requestCode = Utils.randInt(10000, 65535);
		m_simplePermissionCallbacksMap.put(requestCode, callback);
		Activity_HiddenPermissionRequest.start(m_context, permission.getName(), requestCode);
	}


	/**
	 * To be called from the onRequestPermissionsResult method of the Activity or Fragment
	 *
	 * @param permissions  list of permissions
	 * @param grantResults list of results
	 */
	public void onRequestPermissionResult(final int requestCode, final String[] permissions, final int[] grantResults) {
		WHLog.d(TAG, "onRequestPermissionResult");
		WHLog.d(TAG, "requestCode = [ " + requestCode + " ] \npermissions = [ " + Arrays.toString(permissions) + " ] \ngrantResults = [ " + Arrays.toString(grantResults) + " ] \n");

		final SimplePermissionCallbacks simplePermissionCallbacks = m_simplePermissionCallbacksMap.get(requestCode);

		final List<WHPermission> newPermissions = new ArrayList<>(5);
		if ((null != permissions) && (0 != permissions.length) && (null != grantResults) && (0 != grantResults.length)) {
			for (int i = 0; i < permissions.length; i++) {
				final WHPermission permission = m_permissionMap.get(permissions[i]);
				if (null != permission) {
					newPermissions.add(permission);

					if (PERMISSION_DO_NOT_ASK_AGAIN != permission.getState()) {
						final boolean granted = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
						permission.setState(granted ? PERMISSION_GRANTED : PERMISSION_DENIED);
					}
				}

				if (null != simplePermissionCallbacks) {
					switch (permission.getState()) {
						case PERMISSION_DENIED: {
							simplePermissionCallbacks.onPermissionsDenied();
						}
						break;

						case PERMISSION_GRANTED: {
							simplePermissionCallbacks.onPermissionsGranted();
						}
						break;
					}
				}
			}
		}

		for (final PermissionCallbacks permissionCallbacks : m_permissionCallbacks) {
			if (null != permissionCallbacks) {
				for (final WHPermission newPermission : newPermissions) {
					if (newPermission.isGranted()) {
						permissionCallbacks.onPermissionsGranted(requestCode, newPermission);
					}
					else {
						permissionCallbacks.onPermissionsDenied(requestCode, newPermission);
					}
				}
			}
		}

		for (final PermissionCallbacksSingle permissionCallbacksSingle : m_permissionCallbacksSingle) {
			if (null != permissionCallbacksSingle) {
				permissionCallbacksSingle.onPermissionsResult(requestCode, newPermissions);
			}
		}
	}


	/**
	 * Helper that takes in a String
	 *
	 * @param permissionName
	 * @return
	 * @throws PackageManager.NameNotFoundException
	 */
	public boolean needsRequesting(@NonNull final String permissionName) throws PackageManager.NameNotFoundException {
		return needsRequesting(m_permissionMap.get(permissionName));
	}


	/**
	 * @param permission
	 * @return
	 * @throws PackageManager.NameNotFoundException
	 */
	public boolean needsRequesting(@NonNull final WHPermission permission) throws PackageManager.NameNotFoundException {
		if (null == permission) {
			throw new Exception_Permission("WHPermission must not be null.");
		}

		WHLog.d(TAG, "needsRequesting");
		WHLog.d(TAG, "permission = [ " + permission.getName() + " ]");

		final PermissionInfo pi = m_context.getPackageManager().getPermissionInfo(permission.getName(), PackageManager.GET_META_DATA);

		if ((null != pi) && ((pi.protectionLevel & PermissionInfo.PROTECTION_DANGEROUS) == PermissionInfo.PROTECTION_DANGEROUS)) {
			WHLog.d(TAG, "WHPermission " + permission.getName() + " is protection DANGEROUS and DOES require requesting");
			return true;
		}

		WHLog.d(TAG, "WHPermission " + permission.getName() + " is NOT dangerous and does NOT require requesting");
		return false;
	}


	public boolean permissionDeclaredInManifest(final String permission) {
		return permissionDeclaredInManifest(m_context, permission);
	}


	/**
	 * Verifies if the permission is declared in the current application's Manifest
	 *
	 * @param permission the name of the permission
	 * @return true if the permission is declared in the Manifest
	 */
	public static boolean permissionDeclaredInManifest(@NonNull final Context context, final String permission) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
			if (info.requestedPermissions != null) {
				for (String p : info.requestedPermissions) {
					if (p.equals(permission)) {
						return true;
					}
				}
			}
		}
		catch (Exception e) {
			WHLog.e(TAG, e.toString(), e);
		}

		return false;
	}
}
